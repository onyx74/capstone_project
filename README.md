# **This is a capstone project for DevOps internship.** #
## **This ansible script launches gerrit/jenkins/nexus** ## 

## **Keys** 
* All keys are stored in /key directory
* Keys for each role are stored separately in /key/rolename folder
* For gerrit two keys have to be generated (gadmin+gadmin.pub and gjenkins+gjenkins.pub)

## To start: 
1. Pip and ansible have to be installed on your machine
2. Add hosts to hosts file
3. Generate ssh key for your machine `ssh-keygen -t rsa`
4. Copy public key to /home/centos/.ssh/authorized_keys on every machine
5. Execute `ssh-agent bash -c 'ssh-add {{ PATH TO YOUR PRIVATE KEY}}; ansible-playbook -i hosts site.yml -f 10`
